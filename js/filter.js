

export function filterCards(){
    const keyword = document.querySelector('#searchKeyword')
    const searchBtn = document.querySelector('#searchBtn')
    const urgentFilter = document.querySelector('#urgentFilter')
    const statusFilter = document.querySelector('#statusFilter')
    const cards = document.querySelectorAll('.card-wrapper')

    cards.forEach(e=>{
        e.classList.add('d-none')
    })

    if (keyword.value){
        cards.forEach(e=>{
            if(e.innerHTML.toLowerCase().indexOf(keyword.value.toLowerCase()) !== -1){
                e.classList.remove('d-none')
            }
        })
        selectOption(cards, statusFilter.value)
        selectOption(cards, urgentFilter.value)
    }
    else{
        cards.forEach(e=>{
            e.classList.remove('d-none')
        })
        selectOption(cards, statusFilter.value)
        selectOption(cards, urgentFilter.value)
    }

}
function selectOption(elements, value){
    if (value !== 'all'){
        elements.forEach(e=>{
            if(e.innerHTML.toLowerCase().indexOf(value.toLowerCase()) === -1){
                e.classList.add('d-none')
            }
        })
    }
}


