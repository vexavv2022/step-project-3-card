export function validation(params, doc){
    let validParams
    if (doc === 'Терапевт') {
        validParams = ['name', 'purpose', 'age']
    } else if (doc === 'Кардіолог') {
        validParams = ['name', 'purpose', 'age', 'mass', 'pressure', 'diseases']
    } else if (doc === 'Стоматолог') {
        validParams = ['name', 'purpose']
    }
    function domEL (e) {
        return document.getElementById(e)
    }
    let valArr = []
    for (let i = 0; i < validParams.length; i++) {
        let el = domEL(validParams[i])
        let span = document.querySelector(`.${validParams[i]}-message`)
        if (params[validParams[i]].length=== 0) {
            if (!span) {
                const message = document.createElement('span')
                message.textContent = "Введіть дані!"
                el.classList.add('invalid-data')
                message.classList.add(`${validParams[i]}-message`, 'red-mess')
                el.after(message)
            }
            valArr[i] = false
        } else{
            if (span) {
                let sp = domEL(`${validParams[i]}-message`)
                el.classList.remove('invalid-data')
                span.remove()
            }
            valArr[i] =  true
        }
    }

    return valArr.includes(false) ?  false :  true
}
