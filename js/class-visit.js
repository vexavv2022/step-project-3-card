

export class Visit {
    constructor(importance, doctor, name, purpose, description, correctDate, id) {

        this.importance = importance
        this.doctor = doctor
        this.name = name
        this.purpose = purpose
        this.description = description
        this.correctDate = correctDate
        this.id = id
    }
    createCard(){

        const card = ` <div class="card-wrapper" style="width: 25% " ><div class="drag-item" style="width: 100%;">
 <div class="card" id="${this.id}" style="width: 90%;">
            <div class="card-body">
                <h5 class="card-title patient-name"data-name="${this.id}name">${this.name}</h5>
                <span class="registration-num">Запис №${this.id}</span>
                <h5 class="card-title card-doctor"data-name="${this.id}doctor">${this.doctor}</h5>
            </div>
            <button type="button" class="btn-close card-close" id="${this.id}delCard" aria-label="Close"></button>
            <button type="button" class="btn btn-primary btn-showMore" id="${this.id}showMore" data-status="close">Показати більше</button>
               
            <div class="group extended-group" id="${this.id}group">
            <ul class="list-group list-group-flush" id="${this.id}listGroup">
                            <li class="list-group-item"><span class="description-card">Мета візиту: </span> <p data-name="${this.id}purpose">${this.purpose}</p></li>
                            <li class="list-group-item"><span class="description-card">Опис візиту: </span> <p data-name="${this.id}description">${this.description}</p></li>
                            <li class="list-group-item"data-visitStatus="${visitStatus(this.correctDate)}"><span class="description-card">Дата візиту: </span><p data-name="${this.id}correctDate">${this.correctDate}</p></li>
                            <li class="list-group-item visit-priority" data-name="${this.id}importance">${this.importance}</li>
                        </ul>
                        <button type="button" class="btn btn-primary btn-edit" id="${this.id}editBtn">Редагувати</button>
            </div>
            
        </div></div></div>`

        return card
    }
}

function visitStatus (date){
    let status
    let today = new Date()
    let [currYear, currMonth, currDay] = [today.getFullYear(), today.getMonth()+1, today.getDate()]
    let [year, month, day] = [date.slice(0,4), date.slice(5,7), date.slice(8,10)]
    if ((currYear > year) ){
        status = 'done'
        return status
    }
    else if (currMonth > month && (currYear >= year)){
        status = 'done'
        return status

    }
    else if(currDay > day && (currYear >= year) && (currMonth >= month)){
        status = 'done'
        return status
    }
    else {
        status = 'open'
        return status
    }
}