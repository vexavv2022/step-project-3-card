import {VisitCardiologist} from "./class-cardio.js";
import {EditModal} from "./class-modal.js";

export class EntryFieldCardio {
    createFieldCardio() {
        const contentModal = document.querySelector('.content');
        contentModal.insertAdjacentHTML('beforeend', `<form class="entry-field-modal" action="#" method="POST">
    <select class="form-select  modal-input sel form-select-sm" required>
            <option value="" hidden>Терміновість візиту</option>
            <option value="one">Звичайна</option>
            <option value="two">Пріоритетна</option>
            <option value="three">Невідкладна</option>
        </select>
    
    <label for="doctor" class="form-label">Лікар</label>
    <input type="text" id="doctor" class="modal-input" placeholder="Лікар" value="Кардіолог" required readonly>
    <label for="name" class="form-label">ПІБ</label>
    <input type="text"  id="name" class="modal-input"  required>
    <label for="age" class="form-label">Вік</label>
    <input type="number"  id="age" class="modal-input" min="1"  required>
    <label for="pressure" class="form-label">Звичайний тиск</label>
    <input type="text"  id="pressure" class="modal-input"  required>
    <label for="mass" class="form-label">Індекс маси тіла</label>
    <input type="number" id="mass" class="modal-input"  min="1"  required>
    <label for="diseases" class="form-label">Перенесені захворювання</label>
    <input type="text" id="diseases" class="modal-input"  required>
    <label for="purpose" class="form-label">Мета візиту</label>
    <input type="text" id="purpose" class="modal-input"  required>
    <label for="description" class="form-label">Короткий опис візиту</label>
    <textarea type="text" id="description" class="modal-input"></textarea>
    <label for="correct-date" class="form-label">Дата візіту</label>
    <input type="date" id="correct-date" class="modal-input" required>
    <input type="submit" class="add-btn"  value="Додати візит">
</form>`)
    }

    createRequestCardio() {
        const modal = document.querySelector('.modal1')
        const selectImportance = document.querySelector('.sel');
        selectImportance.addEventListener('change', () => {
            selectImportance[selectImportance.selectedIndex].text
        })

        document.querySelector('.add-btn').addEventListener('click', (e) => {
            const importance = selectImportance[selectImportance.selectedIndex].text;
            const doctor = document.querySelector("#doctor").value;
            const name = document.querySelector("#name").value;
            const age = document.querySelector("#age").value;
            const pressure = document.querySelector("#pressure").value;
            const mass = document.querySelector("#mass").value;
            const diseases = document.querySelector("#diseases").value;
            const purpose = document.querySelector("#purpose").value;
            const description = document.querySelector("#description").value;
            const correctDate = document.querySelector("#correct-date").value;
            if ((importance === "Невідкладна" || importance === "Пріоритетна" || importance === "Звичайна") && doctor.length !== 0 && name.length !== 0 && age.length !== 0 && pressure.length !== 0 && mass.length !== 0 && diseases.length !== 0 && purpose.length !== 0 && correctDate.length !== 0) {
                e.preventDefault()
                modal.remove()
                new VisitCardiologist(importance, doctor, name, age, pressure, mass, diseases, purpose, description, correctDate).requestCard()
            }
        })
    }
}

export class EditModalCardio extends EntryFieldCardio {
    EntryFieldCardio(id) {
        new EditModal().createModal()
        super.createFieldCardio()
        document.querySelector(".add-btn").value = 'Зберегти зміни'
        document.querySelector(".add-btn").classList.add('save-changes')
        document.querySelector(".add-btn").classList.remove('add-btn')
        document.querySelector(".sel").firstElementChild.remove()

        let importance = document.querySelector(`[data-name="${id}importance"]`).textContent;
        let index
        if (importance === 'Звичайна') {
            index = 'one'
        }
        if (importance === 'Пріоритетна') {
            index = 'two'
        }
        if (importance === 'Невідкладна') {
            index = 'three'
        }
        document.querySelector(".sel").value = index

        document.querySelector("#doctor").value = document.querySelector(`[data-name="${id}doctor"]`).textContent;
        document.querySelector("#name").value = document.querySelector(`[data-name="${id}name"]`).textContent;
        document.querySelector("#age").value = document.querySelector(`[data-name="${id}age"]`).textContent;
        document.querySelector("#pressure").value = document.querySelector(`[data-name="${id}pressure"]`).textContent;
        document.querySelector("#mass").value = document.querySelector(`[data-name="${id}mass"]`).textContent;
        document.querySelector("#diseases").value = document.querySelector(`[data-name="${id}diseases"]`).textContent;
        document.querySelector("#purpose").value = document.querySelector(`[data-name="${id}purpose"]`).textContent;
        document.querySelector("#description").value = document.querySelector(`[data-name="${id}description"]`).textContent;
        document.querySelector("#correct-date").value = document.querySelector(`[data-name="${id}correctDate"]`).textContent;

    }

    param() {
        let params = new EditModal().params()
        params.age = document.querySelector("#age").value
        params.pressure = document.querySelector("#pressure").value
        params.mass = document.querySelector("#mass").value
        params.diseases = document.querySelector("#diseases").value
        return params
    }
}
