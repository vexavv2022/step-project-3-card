import {Visit} from "./class-visit.js";
import {addCard} from "./script-card.js";

export class VisitTherapist extends Visit {
    constructor(importance, doctor, name, age, purpose, description, correctDate, id) {
        super(importance, doctor, name, purpose, description, correctDate, id);
        this.age = age
        super.createCard()
    }

    requestCard() {
        return  fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'POST',
            body: JSON.stringify({
                importance: `${this.importance}`,
                doctor: `${this.doctor}`,
                name: `${this.name}`,
                age: `${this.age}`,
                purpose: `${this.purpose}`,
                description: `${this.description}`,
                correctDate: `${this.correctDate}`
            }),
            headers: {
                'content-type': 'application/json',
                "authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json())
            .then(data => addCard(data.id))
    }
// додано метод render
    render(){
        let item = document.querySelector('.card-field').innerHTML += this.createCard();
        let extendedCardTherapist = `<li class="list-group-item"><span class="description-card">Вік: </span> <p data-name="${this.id}age">${this.age}</p></li>`
        document.getElementById(this.id + `listGroup`).insertAdjacentHTML('afterbegin', extendedCardTherapist)

        return item;
    }
}