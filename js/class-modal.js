import {EntryFieldTherapist} from './class-entry-field-therapist.js';
import {EntryFieldCardio} from './class-entry-field-cardio.js';
import {EntryFieldDentist} from "./class-entry-field-dentist.js";

export class Modal {
    constructor() {
        this.modal = document.createElement('div');
        this.contentModal = document.createElement('div');
        this.tiitleModal = document.createElement('h2');
        this.closeBtn = document.createElement('span');
    }

    createModal() {
        this.modal.className = 'modal1';
        document.body.append(this.modal);

        this.contentModal.className = 'content';
        this.modal.append(this.contentModal);

        this.tiitleModal.className = 'modal1-title';
        this.tiitleModal.innerText = 'Створити новий візит';
        this.contentModal.append(this.tiitleModal);

        this.closeBtn.className = 'modal1-close';
        this.closeBtn.innerHTML = '&times;';
        this.contentModal.append(this.closeBtn);

        this.contentModal.insertAdjacentHTML('beforeend', `  <select class="form-select add-fields" >
            <option selected>Обрати лікаря</option>
            <option value="one">Кардіолог</option>
            <option value="two">Стоматолог</option>
            <option value="three">Терапевт</option>
        </select>`)


        const select = document.querySelector('.add-fields')
        select.addEventListener('change', () => {
            if (select.value === 'one') {
                select.remove();
                new EntryFieldCardio().createFieldCardio();
                new EntryFieldCardio().createRequestCardio();
            }
            if (select.value === 'two') {
                select.remove();
                new EntryFieldDentist().createFieldDentist();
                new EntryFieldDentist().createRequestDentist();
            }
            if (select.value === 'three') {
                select.remove();
                new EntryFieldTherapist().createFieldTherapist();
                new EntryFieldTherapist().createRequestTherapist();
            }
        })
        const myModal = document.querySelector('.modal1')
        const closeBtn = document.querySelector('.modal1-close')
        window.addEventListener('click', (event) => {
            if (event.target === myModal || event.target === closeBtn) {
                myModal.remove()
            }
        })
    }

}

export class EditModal extends Modal{

    createModal(){
        super.createModal()

        this.tiitleModal.innerText = 'Редагувати';
        document.querySelector('.add-fields').remove()
    }
    params(){
        return  {
            importance: document.querySelector(".sel")[document.querySelector(".sel").selectedIndex].text,
            doctor: document.querySelector("#doctor").value,
            name: document.querySelector("#name").value,
            purpose: document.querySelector("#purpose").value,
            description: document.querySelector("#description").value,
            correctDate: document.querySelector("#correct-date").value
        }
    }
}
