import {VisitCardiologist} from "./class-cardio.js";
import {VisitDentist} from "./class-dentist.js";
import {VisitTherapist} from "./class-therapist.js";
import {drag} from "./drag&drop.js";

export async function downloadCards (){
    const responseCards = await fetch(`https://ajax.test-danit.com/api/v2/cards`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            "authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
    const cards = await responseCards.json()
    let result = cards.sort((a,b)=>a.id-b.id).map(async(card) =>{
        await chooseDoctor(card)
    })
}

export async function addCard(cardId){
    const responseCards = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            "authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
    const card = await responseCards.json()
    await chooseDoctor(card)
    await drag()
}

export async function deleteCard(cardId){
    return await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            "authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
}
//
function chooseDoctor(card){
    if (card.doctor === "Терапевт"){
        return (new VisitTherapist(card.importance, card.doctor, card.name, card.age, card.purpose, card.description, card.correctDate,card.id)).render()
    } else if (card.doctor === "Кардіолог"){
        return (new VisitCardiologist(card.importance, card.doctor, card.name, card.age, card.pressure, card.mass, card.diseases, card.purpose, card.description, card.correctDate,card.id)).render()
    } else if (card.doctor === "Стоматолог"){
        return (new VisitDentist(card.importance, card.doctor, card.name, card.lastVisit, card.purpose, card.description, card.correctDate,card.id)).render()
    }
}

export async function editCard(cardId, cardDoctor, {importance, doctor, name, age, purpose, description, correctDate, lastVisit, mass, pressure, diseases}){
    let changeParams = {
        id: cardId,
        importance: importance,
        doctor: doctor,
        name: name,
        purpose: purpose,
        description: description,
        correctDate: correctDate
    }
    if (cardDoctor === "Терапевт"){
        changeParams.age = age
    }else if (cardDoctor === "Кардіолог"){
        changeParams.age = age
        changeParams.mass = mass
        changeParams.pressure = pressure
        changeParams.diseases = diseases
    } else if (cardDoctor === "Стоматолог"){
        changeParams.lastVisit = lastVisit
    }

    const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(changeParams)
    })
    const result = await response.json()
    updateCard(result.id, result.doctor, result)

}


function updateCard(cardId, cardDoctor, changeParams){
    for (const argument in changeParams) {
        if (cardDoctor === "Терапевт") {
            if (argument !== ('id' || 'mass' || 'pressure' || 'diseases' || 'lastVisit')) {
                document.querySelector(`[data-name="${cardId}${argument}"]`).innerText = changeParams[argument]
            }
        } else if (cardDoctor === "Кардіолог") {
            if (argument !== ('id' || 'lastVisit')) {
                document.querySelector(`[data-name="${cardId}${argument}"]`).textContent = changeParams[argument]
            }
        } else if (cardDoctor === "Стоматолог") {
            if (argument !== ('id' || 'mass' || 'pressure' || 'diseases')) {
                document.querySelector(`[data-name="${cardId}${argument}"]`).textContent = changeParams[argument]
            }
        }
    }

}