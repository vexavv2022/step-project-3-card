import {Modal, EditModal} from './class-modal.js';
import {downloadCards, deleteCard, editCard, addCard} from './script-card.js'
import {EditModalCardio, EntryFieldCardio} from "./class-entry-field-cardio.js";
import {EntryFieldTherapist, EditModalTherapist} from './class-entry-field-therapist.js'
import {filterCards} from './filter.js'
import {EditModalDentist} from "./class-entry-field-dentist.js";
import {drag} from "./drag&drop.js";
import {cardsChecker} from './cards-checker.js';
import {advertisingWindow} from './advertising-window.js';
import {validation} from "./form-validation.js";

document.querySelector('.new-visit').addEventListener('click', () => {
    new Modal().createModal()
})


window.addEventListener("load", async () => {
    await downloadCards()
    await drag()
    cardsChecker()
})

// <------------------ close advertising modal
setTimeout(advertisingWindow, 15000)

let add = document.querySelector('.doctor-fixed')
let closeAdd = document.querySelector('.button-close')
closeAdd.addEventListener('click', () => {
    add.style.display = 'none'
});


document.addEventListener('click', async (e) => {
    let id = parseInt(e.target.id)
    if (e.target.classList.contains('btn-showMore')) {
        if (e.target.getAttribute('data-status') === 'close') {
            e.target.textContent = 'Згорнути'
            document.getElementById(id + 'group').classList.toggle('extended-group')
            e.target.setAttribute('data-status', 'active')
        } else {
            document.getElementById(id + 'group').classList.toggle('extended-group')
            e.target.textContent = 'Показати більше'
            e.target.setAttribute('data-status', 'close')
        }
    }
    if (e.target.classList.contains('card-close')) {
        await deleteCard(id)
        document.getElementById(id).parentNode.parentNode.remove()
    }
    if (e.target.classList.contains('btn-edit')) {
        let doc = document.querySelector(`[data-name="${id}doctor"]`).innerText
        console.log(doc)
        if (doc === 'Терапевт') {
            new EditModalTherapist().createFieldTherapist(id)
        } else if (doc === 'Кардіолог') {
            new EditModalCardio().EntryFieldCardio(id)
        } else if (doc === 'Стоматолог') {
            new EditModalDentist().EntryFieldDentist(id)
        }
        document.querySelector('.save-changes').addEventListener('click', async (el) => {
            el.preventDefault()
            let params
            if (doc === 'Терапевт') {
                params = new EditModalTherapist().param()
            } else if (doc === 'Кардіолог') {
                params = new EditModalCardio().param()
            } else if (doc === 'Стоматолог') {
                params = new EditModalDentist().param()
            }
            if (validation(params, doc)) {
                await editCard(id, params.doctor, params)
                document.querySelector('.modal1').remove()
            }

        })
    }
    if (e.target.closest('#searchBtn')){
        e.preventDefault()
        filterCards()
    }
})





