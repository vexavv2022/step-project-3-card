import {Visit} from "./class-visit.js";
import {addCard} from "./script-card.js";

export class VisitDentist extends Visit {
    constructor(importance, doctor, name, lastVisit, purpose, description, correctDate, id) {
        super(importance, doctor, name, purpose, description, correctDate, id);
        this.lastVisit = lastVisit
        super.createCard()
    }

    requestCard() {
        return fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'POST',
            body: JSON.stringify({
                importance: `${this.importance}`,
                doctor: `${this.doctor}`,
                name: `${this.name}`,
                lastVisit: `${this.lastVisit}`,
                purpose: `${this.purpose}`,
                description: `${this.description}`,
                correctDate: `${this.correctDate}`
            }),
            headers: {
                'content-type': 'application/json',
                "authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json())
            .then(data => addCard(data.id))
    }
    render(){
        let item = document.querySelector('.card-field').innerHTML += this.createCard();
        let extendedCardTherapist = `<li class="list-group-item"><span class="description-card">Дата останнього візиту:</span> <p data-name="${this.id}lastVisit">${this.lastVisit}</p></li>`
        document.getElementById(this.id + `listGroup`).insertAdjacentHTML('afterbegin', extendedCardTherapist)

        return item;
    }
}
