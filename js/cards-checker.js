export function cardsChecker() {


    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },

    })
        .then(response => response.json())
        .then(cards => {
            let createCards = document.createElement('h1')
            createCards.classList.add('no-items')
            createCards.textContent = 'В данний момент записів немає.'
            if (cards.length === 0) {

                document.querySelector('.field').append(createCards)
            }
            if (cards.length > 0) {
                createCards.remove()
            }
        });
}


