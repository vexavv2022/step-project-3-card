import {VisitTherapist} from "./class-therapist.js";
import {EditModal} from './class-modal.js'

export class EntryFieldTherapist {
    createFieldTherapist() {
        const contentModal = document.querySelector('.content');
        contentModal.insertAdjacentHTML('beforeend', `<form class="entry-field-modal" action="#" method="POST">
    <select class="form-select  modal-input sel form-select-sm" required>
            <option value="" hidden>Терміновість візиту</option>
            <option value="one">Звичайна</option>
            <option value="two">Пріоритетна</option>
            <option value="three">Невідкладна</option>
        </select>
    <label for="doctor" class="form-label">Лікар</label>
    <input type="text" id="doctor" class="modal-input"  value="Терапевт" required readonly>
    <label for="name" class="form-label">ПІБ</label>
    <input type="text"  id="name" class="modal-input"  required>
    <label for="age" class="form-label">Вік</label>
    <input type="number"  id="age" class="modal-input" min="1"  required>
    <label for="purpose" class="form-label">Мета візиту</label>
    <input type="text" id="purpose" class="modal-input"  required>
    <label for="description" class="form-label">Короткий опис візиту</label>
    <textarea type="text" id="description" class="modal-input"></textarea>
    <label for="correct-date" class="form-label">Дата візіту</label>
    <input type="date" id="correct-date" class="modal-input" required>
    <input type="submit" class="add-btn"  value="Додати візит">
</form>`)
    }

    createRequestTherapist() {
        const modal = document.querySelector('.modal1')
        const selectImportance = document.querySelector('.sel');
        selectImportance.addEventListener('change', () => {
            selectImportance[selectImportance.selectedIndex].text
        })
        document.querySelector('.add-btn').addEventListener('click', (e) => {

            const importance = selectImportance[selectImportance.selectedIndex].text;
            const doctor = document.querySelector("#doctor").value;
            const name = document.querySelector("#name").value;
            const age = document.querySelector("#age").value;
            const purpose = document.querySelector("#purpose").value;
            const description = document.querySelector("#description").value;
            const correctDate = document.querySelector("#correct-date").value;
            if ((importance === "Невідкладна" || importance === "Пріоритетна" || importance === "Звичайна") && doctor.length !== 0 && name.length !== 0 && age.length !== 0 && purpose.length !== 0 && correctDate.length !== 0) {
                e.preventDefault()
                modal.remove()
                new VisitTherapist(importance, doctor, name, age, purpose, description, correctDate).requestCard();
            }
        })
    }

}

export class EditModalTherapist extends EntryFieldTherapist {
    createFieldTherapist(id) {
        new EditModal().createModal()
        super.createFieldTherapist()
        document.querySelector(".add-btn").value = 'Зберегти зміни'
        document.querySelector(".add-btn").classList.add('save-changes')
        document.querySelector(".add-btn").classList.remove('add-btn')
        document.querySelector(".sel").firstElementChild.remove()

         let importance = document.querySelector(`[data-name="${id}importance"]`).textContent;
         let index
        if (importance === 'Звичайна') {
            index = 'one'
        }
        if (importance === 'Пріоритетна') {
            index = 'two'
        }
        if (importance === 'Невідкладна') {
            index = 'three'
        }
        document.querySelector(".sel").value = index
        document.querySelector("#doctor").value = document.querySelector(`[data-name="${id}doctor"]`).textContent;
        document.querySelector("#name").value = document.querySelector(`[data-name="${id}name"]`).textContent;
        document.querySelector("#age").value = document.querySelector(`[data-name="${id}age"]`).textContent;
        document.querySelector("#purpose").value = document.querySelector(`[data-name="${id}purpose"]`).textContent;
        document.querySelector("#description").value = document.querySelector(`[data-name="${id}description"]`).textContent;
        document.querySelector("#correct-date").value = document.querySelector(`[data-name="${id}correctDate"]`).textContent;

    }

    param() {
        let params = new EditModal().params()
        params.age = document.querySelector("#age").value
        return params
    }
}