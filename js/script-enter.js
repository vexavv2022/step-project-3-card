'use strict';
function getData(email, password) {
    fetch('https://ajax.test-danit.com/api/v2/cards/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: email, password: password})
    })
        .then(response => {
            if(response.ok){
                return response.text()
            }else{
                alert('Введіть правильний email або пароль!')
            }
        }).then(token => {
        localStorage.setItem('token',token);
        let admin = localStorage.getItem('token')
        if(token === admin){
            document.location.href = ('./html/form.html')
        }else{
            document.location.href = ('./index.html')
        }
    })

}
document.querySelector('#send-data').addEventListener('click', (e) => {
    e.preventDefault();
    let email = document.querySelector('#input-email');
    let password = document.querySelector('#input-password');
    getData(email.value, password.value )
})


// export class Login {
//     constructor() {
//        this._email = document.querySelector('#input-email').value;
//         this._password = document.querySelector('#input-password').value;
//
//     }
//     enter(){
//         return fetch('https://ajax.test-danit.com/api/v2/cards/login', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({email: this._email, password: this._password})
//         })
//             .then(response => {
//                 if(response.ok){
//                     console.log('Welcome!')
//                     return response.text()
//                 }else{
//                     alert('Enter correct email or password!')
//                 }
//             }).then(token => {
//                 localStorage.setItem('token',token);
//                 let admin = localStorage.getItem('token')
//                 if(token === admin){
//                     document.location.href = ('./html/form.html')
//                 }else{
//                     document.location.href = ('./index.html')
//                 }
//             })
//
//     }
//
// }
// document.querySelector('#send-data').addEventListener('click', (e) => {
//     e.preventDefault();
//     new Login().enter()
//
// })