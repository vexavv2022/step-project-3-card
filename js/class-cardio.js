import {Visit} from "./class-visit.js";
import {addCard} from "./script-card.js";

export class VisitCardiologist extends Visit {
    constructor(importance, doctor, name, age, pressure, mass, diseases, purpose, description, correctDate,id) {
        super(importance, doctor, name, purpose, description, correctDate, id);
        this.age = age
        this.pressure = pressure
        this.mass = mass
        this.diseases = diseases
        super.createCard()
    }

    requestCard() {
        return fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'POST',
            body: JSON.stringify({
                importance: `${this.importance}`,
                doctor: `${this.doctor}`,
                name: `${this.name}`,
                age: `${this.age}`,
                pressure: `${this.pressure}`,
                mass: `${this.mass}`,
                diseases: `${this.diseases}`,
                purpose: `${this.purpose}`,
                description: `${this.description}`,
                correctDate: `${this.correctDate}`
            }),
            headers: {
                'content-type': 'application/json',
                "authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json())
            .then(data => addCard(data.id))
    }
    render(){
        let item = document.querySelector('.card-field').innerHTML += this.createCard();
        let extendedCardTherapist = `<li class="list-group-item"><span class="description-card">Вік: </span><p data-name="${this.id}age">${this.age}</p></li>
                                    <li class="list-group-item" ><span class="description-card">Звичайний тиск: </span> <p data-name="${this.id}pressure">${this.pressure}</p></li>
                                    <li class="list-group-item"><span class="description-card">ІМТ: </span><p data-name="${this.id}mass">${this.mass}</p></li>
                                    <li class="list-group-item" ><span class="description-card">Попередні захворювання: </span> <p data-name="${this.id}diseases">${this.diseases}</p></li>`
        document.getElementById(this.id + `listGroup`).insertAdjacentHTML('afterbegin', extendedCardTherapist)

        return item;
    }
}
