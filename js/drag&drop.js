
export async  function drag() {
    let newPosX = 0;
    let newPosY = 0;
    let startPosX = 0;
    let startPosY = 0;
    let elemId = 0;
    let index = 10;
    document.querySelectorAll('.drag-item').forEach(el => {


        el.addEventListener('mousedown', function(e){

            if (e.target.closest('button')) {
               return
            }

            e.preventDefault();
            startPosX = e.clientX;
            startPosY = e.clientY;
;

            document.addEventListener('mousemove', mouseMove);

            document.addEventListener('mouseup', function(){
                document.removeEventListener('mousemove', mouseMove);
            });

        });


        function mouseMove(e) {
            if (newPosX !== startPosX && newPosY !== startPosY){
            index += 1;
            el.style.zIndex = index;
            el.style.position = 'absolute'
            newPosX = startPosX - e.clientX;
            newPosY = startPosY - e.clientY;

            startPosX = e.clientX;
            startPosY = e.clientY;
            el.style.top = (el.offsetTop - newPosY) + "px";
            el.style.left = (el.offsetLeft - newPosX) + "px";
            }
        }

        el.ondragstart = function () {
            return false;
        };
    })
}


